import 'package:flutter/material.dart';

class PredictionGridTile extends StatelessWidget {
  const PredictionGridTile(
      {Key key,
        @required this.title,
        @required this.image,
        @required this.content,
        @required this.footerOnTap})
      : super(key: key);

  final String title;
  final String image;
  final String content;
  final Function footerOnTap;

  @override
  Widget build(BuildContext context) {
    return GridTile(
      header: Text(
        '${title.toUpperCase()}',
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.lightGreenAccent),
      ),
      child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage(image), fit: BoxFit.fitWidth),
              border:
              Border.all(width: 5, color: Colors.white.withOpacity(0.1)),
              borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Center(
              child: Text(
                '${content.substring(0, content.length ~/ 2)} ...',
                textAlign: TextAlign.center,
              ),
            ),
          )),
      footer: GestureDetector(
        onTap: footerOnTap,
        child: Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              'Read More  >>',
              textAlign: TextAlign.end,
            )),
      ),
    );
  }
}
