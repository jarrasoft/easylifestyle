import 'package:flutter/material.dart';

class LifestyleDrawerItem extends StatelessWidget {
  final String title;
  final Function onPressed;
  final EdgeInsetsGeometry margin;
  final Color color;

  const LifestyleDrawerItem({Key key, this.title, this.onPressed,
    this.margin = const EdgeInsets.only(left: 10, right: 10, bottom: 10),
    this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      color: color ?? Colors.white.withOpacity(0.5),
      child: ListTile(
        title: Text(
          '$title',
          style: Theme.of(context)
              .textTheme
              .bodyText2
              .copyWith(color: Colors.black),
        ),
        onTap: onPressed,
      ),
    );
  }
}