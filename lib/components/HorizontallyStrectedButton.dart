import 'package:flutter/material.dart';

class HorizontallyStretchedButton extends StatelessWidget {
  const HorizontallyStretchedButton(this.text, {Key key, this.onPressed})
      : super(key: key);

  final Function onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
          side: BorderSide(
            color: Colors.black.withOpacity(0.8),
            width: 5
          )
        ),
        onPressed: onPressed,
        child: Text(
          '$text',
          style: Theme.of(context).textTheme.bodyText2.copyWith(
            color: Colors.black
          ),
        ),
      ),
    );
  }
}
