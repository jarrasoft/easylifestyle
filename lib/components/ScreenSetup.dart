import 'package:flutter/material.dart';

class ScreenSetup extends StatelessWidget {
  const ScreenSetup(
      {Key key,
      this.decoration = const BoxDecoration(color: Color(0xFF050B18)),
      this.child,
      this.appBar,
      this.drawer,
      this.scaffoldKey, this.bottomNavigationBar})
      : super(key: key);

  final Decoration decoration;
  final Widget child;
  final Widget appBar;
  final Widget drawer;
  final Widget bottomNavigationBar;
  final GlobalKey scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      child: SafeArea(
        child: Scaffold(
          key: scaffoldKey,
          appBar: appBar,
          backgroundColor: Colors.transparent,
          body: Container(child: child),
          drawer: drawer,
          bottomNavigationBar: bottomNavigationBar,
        ),
      ),
    );
  }
}
