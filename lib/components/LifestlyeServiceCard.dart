import 'package:flutter/material.dart';

class LifestyleServiceCard extends StatelessWidget {
  const LifestyleServiceCard({Key key, @required this.title, this.onPressed})
      : super(key: key);

  final String title;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.black.withOpacity(0.7),
        margin: EdgeInsets.all(10),
        child: Center(
          child: SizedBox(
            width: 120,
            child: Text(
              title,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}