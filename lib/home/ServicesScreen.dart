import 'dart:io';

import 'package:easylifestyle/components/LifestlyeServiceCard.dart';
import 'package:easylifestyle/components/LifestyleDrawerItem.dart';
import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/components/Spaces.dart';
import 'package:easylifestyle/constants/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Service {
  String name;
  Function onPressed;

  Service(this.name, this.onPressed);
}

class ServiceScreen extends StatelessWidget {
  final List<Service> services = [
    Service("Find My Friends", (context) {
      Navigator.pushNamed(context, '/predictions');
    }),
    Service("Predictions", (context) {
      Navigator.pushNamed(context, '/predictions');
    }),
    Service("Chat", (context) {
      Navigator.pushNamed(context, '/chat');
    }),
    Service("Play Games", (context) {
      Navigator.pushNamed(context, '/predictions');
    }),
    Service("Trending on Twitter", (context) {
      Navigator.pushNamed(context, '/predictions');
    }),
    Service("Spending Analysis", (context) {
      Navigator.pushNamed(context, '/predictions');
    }),
    Service("Favorite Songs", (context) {
      Navigator.pushNamed(context, '/predictions');
    })
  ];

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: ScreenSetup(
        scaffoldKey: _scaffoldKey,
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.black.withOpacity(0.5),
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _scaffoldKey.currentState.openDrawer();
                    },
                    child: Icon(
                      Icons.menu,
                      color: Colors.white,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Mary Doe',
                        style: TextStyle(fontSize: 15),
                      ),
                      HorizontalSpace(10),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed('/profilemanagement');
                        },
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: Image.asset(
                            AssetImages.avatar,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(15),
                child: GridView.builder(
                  itemCount: services.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemBuilder: (BuildContext _context, int index) {
                    return LifestyleServiceCard(
                      title: services[index].name,
                      onPressed: () {
                        services[index].onPressed(context);
                      },
                    );
                  },
                ),
              ),
            ),
          ],
        ),
        drawer: Drawer(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.8),
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: ExactAssetImage(AssetImages.drawerBackground2))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    LifestyleDrawerItem(
                      title: 'Profile Management',
                      onPressed: () {
                        Navigator.pushNamed(context, '/profilemanagement');
                      },
                      margin: EdgeInsets.all(10),
                    ),
                    LifestyleDrawerItem(
                        title: 'Subscription',
                        onPressed: () {
                          Navigator.pushNamed(context, "/subscription");
                        }),
                    LifestyleDrawerItem(
                        title: 'Settings',
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                  ],
                ),
                LifestyleDrawerItem(
                    title: 'Log Out',
                    color: Colors.greenAccent,
                    onPressed: () {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) {
                            return AlertDialog(
                              title: Text(''),
                              content: Text('Are you sure you want to logout?'),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('YES'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    exit(0);
                                  },
                                ),
                                FlatButton(
                                  child: Text('NO'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            );
                          });
                    }),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.person_outline),
                onPressed: () {
                  Navigator.of(context).pushNamed('/profilemanagement');
                },
              ),
              IconButton(
                icon: Icon(Icons.vpn_lock),
                onPressed: () {
                  Navigator.of(context).pushNamed('/subscription');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
