class AssetImages {

  // MY MAIN IMAGES
  static const String avatar = 'images/avatar.jpg';
  static const String drawerBackground = 'images/drawer-background.jpg';
  static const String drawerBackground2 = 'images/drawer-background2.jpg';
  static const String background = 'images/background.png';
  static const String mask = 'images/mask.png';

  // OTHER IMAGES
  static const String alexiRomani = "images/alexi-romano.jpg";

}

class NetworkImages {

}