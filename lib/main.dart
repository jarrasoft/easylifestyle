import 'package:easylifestyle/chat/ChatScreen.dart';
import 'package:easylifestyle/home/ServicesScreen.dart';
import 'package:easylifestyle/predictions/PredictionScreen.dart';
import 'package:easylifestyle/predictions/PredictionsHomeScreen.dart';
import 'package:easylifestyle/profilemanagement/ProfileManagementScreen.dart';
import 'package:easylifestyle/registration/screens/OtpVerificationScreen.dart';
import 'package:easylifestyle/registration/screens/UserDetailsInputScreen.dart';
import 'package:easylifestyle/registration/screens/WelcomeScreen.dart';
import 'package:easylifestyle/subscription/SubscriptionScreen.dart';
import 'package:easylifestyle/utilities/LifestlyeSharedPreferences.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LifestyleSharedPreferences.setUp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Easy Lifestyle',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Color(0xFF6C55E1),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          buttonTheme: ButtonThemeData(
            buttonColor: Colors.white,
            textTheme: ButtonTextTheme.primary,
          ),
          appBarTheme: AppBarTheme(
            color: Colors.transparent,
            textTheme: TextTheme(
              headline6: GoogleFonts.tenaliRamakrishna(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ),
          textTheme: TextTheme(
            bodyText2: GoogleFonts.kanit(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.white,
            ),
          ),
          inputDecorationTheme: InputDecorationTheme(
            fillColor: Colors.white,
            hintStyle: GoogleFonts.tenaliRamakrishna(),

          ),
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        "/welcome": (context) => WelcomeScreen(),
        "/userinput": (context) => UserDetailInputScreen(),
        "/verifyotp": (context) => OTPVerificationScreen(),
        "/services": (context) => ServiceScreen(),
        "/predictions": (context) => PredictionsHomeScreen(),
        "/profilemanagement": (context) => ProfileManagementScreen(),
        "/chat": (context) => ChatScreen(),
        "/subscription": (context) => SubscriptionScreen(),
      },
      initialRoute: !LifestyleSharedPreferences.userOnboardedSuccessfully ? "/services" : '/welcome',
      // ignore: missing_return
      onGenerateRoute: (settings){
        if(settings.name == '/prediction'){
          final Prediction prediction = settings.arguments;
          return MaterialPageRoute(
            builder: (context){
              return PredictionScreen(
                prediction: prediction,
              );
            },
          );
        }
      },
    );
  }
}
