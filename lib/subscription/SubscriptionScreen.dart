import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:flutter/material.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(
        title: Text('Subscription'),
      ),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Card(
              elevation: 5,
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.all(10),
                height: 200,
                width: double.infinity,
                child: Column(
                  children: <Widget>[
                    Text(
                      'BRONZE',
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      'Predictions',
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      'Play Games',
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      'Spending Analysis',
                      style: TextStyle(color: Colors.black),
                    )
                  ],
                ),
              ),
              margin: EdgeInsets.all(15),
            ),
          ],
        ),
      ),
    );
  }
}
