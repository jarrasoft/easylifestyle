import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/components/Spaces.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OTPVerificationScreen extends StatefulWidget {
  @override
  _OTPVerificationScreenState createState() => _OTPVerificationScreenState();
}

class _OTPVerificationScreenState extends State<OTPVerificationScreen> {
  TextEditingController _firstDigit = TextEditingController();
  TextEditingController _secondDigit = TextEditingController();
  TextEditingController _thirdDigit = TextEditingController();
  TextEditingController _forthDigit = TextEditingController();
  TextEditingController _fifthDigit = TextEditingController();

  FocusNode _firstFocusNode = FocusNode();
  FocusNode _secondFocusNode = FocusNode();
  FocusNode _thirdFocusNode = FocusNode();
  FocusNode _forthfFocusNode = FocusNode();
  FocusNode _fifthFocusNode = FocusNode();

  bool showProgressIndicator = false;
  String otpVerificationMessage;

  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(
        title: Text('OTP'),
      ),
      child: Container(
        color: Colors.white.withOpacity(0.6),
        child: showProgressIndicator
            ? Center(
                child: LinearProgressIndicator(),
              )
            : Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Enter your OTP',
                    ),
                    VerticalSpace(20),
                    Row(
                      children: <Widget>[
                        HorizontalSpace(10),
                        Expanded(
                          child: CupertinoTextField(
                            controller: _firstDigit,
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                            autofocus: true,
                            onChanged: (val) {
                              print('value$val');
                              if(val.isEmpty || val.length < 1){
                                _firstFocusNode.requestFocus();
                              }
                              else{
                                _firstDigit.text = val[val.length-1];
                                _secondFocusNode.requestFocus();
                              }
                            },
                            focusNode: _firstFocusNode,
                            //showCursor: false,
                          ),
                        ),
                        HorizontalSpace(10),
                        Expanded(
                            child: CupertinoTextField(
                              focusNode: _secondFocusNode,
                              keyboardType: TextInputType.number,
                              maxLength: 2,
                          controller: _secondDigit,
                              onChanged: (val){
                                if(val.isEmpty || val.length < 1){
                                  _firstFocusNode.requestFocus();
                                }
                                else{
                                  _secondDigit.text = val[val.length-1];
                                  _thirdFocusNode.requestFocus();
                                }
                              },
                              //showCursor: false,
                        )),
                        HorizontalSpace(10),
                        Expanded(
                            child: CupertinoTextField(
                          controller: _thirdDigit,
                              keyboardType: TextInputType.number,
                              focusNode: _thirdFocusNode,
                                maxLength: 2,
                              onChanged: (val){
                                if(val.isEmpty || val.length < 1){
                                  _secondFocusNode.requestFocus();
                                }
                                else{
                                  _thirdDigit.text = val[val.length-1];
                                  _forthfFocusNode.requestFocus();
                                }
                              },
                              //showCursor: false,
                        )),
                        HorizontalSpace(10),
                        Expanded(
                            child: CupertinoTextField(
                          controller: _forthDigit,
                              keyboardType: TextInputType.number,
                              focusNode: _forthfFocusNode,
                                maxLength: 2,
                              onChanged: (val){
                                if(val.isEmpty || val.length < 1){
                                  _thirdFocusNode.requestFocus();
                                }
                                else{
                                  _forthDigit.text = val[val.length-1];
                                  _fifthFocusNode.requestFocus();
                                }
                              },
                              //showCursor: false,
                        )),
                        HorizontalSpace(10),
                        Expanded(
                          child: CupertinoTextField(
                            controller: _fifthDigit,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              setState(() {
                                otpVerificationMessage = "";
                              });
                              if(val.isEmpty){
                                _forthfFocusNode.requestFocus();
                              }
                              else{
                                _fifthDigit.text = val[val.length-1];
                                verificationLogicHandler();
                              }

                            },
                            focusNode: _fifthFocusNode,
                            maxLength: 2,
                            //showCursor: false,
                          ),
                        ),
                        HorizontalSpace(10),
                      ],
                    ),
                    VerticalSpace(20),
                    Text(otpVerificationMessage ?? ''),
                  ],
                ),
              ),
      ),
    );
  }

  verificationLogicHandler() {
    String otp = '${_firstDigit.text}'
        '${_secondDigit.text}${_thirdDigit.text}'
        '${_forthDigit.text}${_fifthDigit.text}';

    if (otp.isEmpty || otp.length < 5) {
      return;
    } else {
      bool otpIsValid = verifyOTP(otp);

      if (otpIsValid) {
        Navigator.pushNamed(context, '/services');
      } else {
        setState(() {
          otpVerificationMessage = "Verification Failed";
        });
      }
    }
  }

  bool verifyOTP(String otp) {
    setState(() {
      showProgressIndicator = true;
    });

    print('Verifying $otp');

    setState(() {
      showProgressIndicator = false;
    });

    return otp == "12345";
  }
}
