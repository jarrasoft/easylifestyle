import 'package:easylifestyle/components/HorizontallyStrectedButton.dart';
import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserDetailInputScreen extends StatefulWidget {
  @override
  _UserDetailInputScreenState createState() => _UserDetailInputScreenState();
}

class _UserDetailInputScreenState extends State<UserDetailInputScreen> {

  TextEditingController _phoneNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(
        title: Text('Input Number'),
      ),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                alignment: Alignment.center,
                color: Colors.white.withOpacity(0.6),
                padding: const EdgeInsets.all(40.0),
                child: CupertinoTextField(
                  controller: _phoneNumberController,
                  placeholder: 'Type in your phone number',
                  keyboardType: TextInputType.phone,
                  maxLength: 12,
                  placeholderStyle: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Colors.black.withOpacity(0.3)
                  ),
                ),
              ),
            ),
            HorizontallyStretchedButton(
              'CONFIRM',
              onPressed: () {
                print(_phoneNumberController.text);
                Navigator.pushNamed(context, '/verifyotp');
              },
            )
          ],
        ),
      ),
    );
  }
}
