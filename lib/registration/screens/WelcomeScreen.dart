import 'package:easylifestyle/components/HorizontallyStrectedButton.dart';
import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/constants/strings.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      'Welcome to Easy Lifestyle',
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Image.asset(
                AssetImages.background,
                fit: BoxFit.cover,
                color: Colors.black.withOpacity(0.6),
                colorBlendMode: BlendMode.darken,
              ),
            ),
            Container(
              child: HorizontallyStretchedButton(
                'PROCEED',
                onPressed: () {
                  Navigator.pushNamed(context, '/userinput');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
