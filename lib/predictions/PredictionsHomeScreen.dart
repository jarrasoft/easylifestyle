import 'package:easylifestyle/components/PredictionGridTile.dart';
import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/components/Spaces.dart';
import 'package:easylifestyle/constants/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Prediction {
  final String title;
  final String content;
  final String image;

  Prediction(this.title, this.content, this.image);
}

class PredictionsHomeScreen extends StatefulWidget {
  @override
  _PredictionsHomeScreenState createState() => _PredictionsHomeScreenState();
}

class _PredictionsHomeScreenState extends State<PredictionsHomeScreen> {
  final List<Prediction> predictions = [
    Prediction(
        'Forex will crash',
        'Forex will crash because the world keeps '
            'revolving around the fact that we are operating'
            ' a currency system that does not benefit everyone.',
        AssetImages.drawerBackground),
    Prediction(
        'Money will finish',
        'Your money will finish because you spend it on thing you dont need. '
            'Your money will finish and your money will never be enough because'
            ' change is constant',
        AssetImages.drawerBackground2),
    Prediction(
        'Robots will take over',
        'Robots will take over because humans are lazy. We make robots do all'
            ' our stuffs and soon robots will take all our stuffs from us.'
            ' We make robots smart and soon they will be smarter',
        AssetImages.drawerBackground),
    Prediction(
        'Humans will fight',
        'Humans will fight because they cant think of anything better to do',
        AssetImages.drawerBackground2),
    Prediction(
        'Developers will lose their jobs',
        'Developers will lose their jobs because of AI. Developers are'
            ' lazy people. They build software to write software, they '
            'keep finding new ways to be lazy and this is why they will '
            'lose their jobs.',
        AssetImages.drawerBackground),
  ];

  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(
        title:
            Text('Predictions for ${DateTime.now().toString().split(' ')[0]}'),
      ),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            VerticalSpace(10),
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: GridView.builder(
                    itemCount: predictions.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1, mainAxisSpacing: 20),
                    itemBuilder: (context, index) {
                      var prediction = predictions[index];

                      var title = prediction.title;
                      var image = prediction.image;
                      var content = prediction.content;

                      return PredictionGridTile(
                        title: title,
                        image: image,
                        content: content,
                        footerOnTap: () {
                          Navigator.of(context)
                              .pushNamed('/prediction', arguments: prediction);
                        },
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
