import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/predictions/PredictionsHomeScreen.dart';
import 'package:flutter/material.dart';

class PredictionScreen extends StatelessWidget {
  final Prediction prediction;

  const PredictionScreen({Key key, this.prediction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(
        title: Text('Prediction'),
      ),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Text('${prediction.content}'),
      ),
    );
  }
}
