import 'package:shared_preferences/shared_preferences.dart';

class LifestyleSharedPreferences {

  static SharedPreferences _preferences;

  static setUp() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static bool get userOnboardedSuccessfully =>
      _preferences.getBool('uos') ?? false;

  static set userOnboardedSuccessfully(bool value){
    _preferences.setBool('uos', value);
  }

}