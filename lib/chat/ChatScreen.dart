import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/components/Spaces.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      appBar: AppBar(title: Text('CHAT'),),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.white,
            ),
          ),
          Row(children: <Widget>[
            Expanded(child: CupertinoTextField()),
            HorizontalSpace(10),
            Icon(Icons.send, color: Colors.greenAccent,),
            HorizontalSpace(10),
          ],)
        ],
      ),
    );
  }
}
