import 'package:easylifestyle/components/ScreenSetup.dart';
import 'package:easylifestyle/components/Spaces.dart';
import 'package:easylifestyle/constants/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileManagementScreen extends StatefulWidget {
  @override
  _ProfileManagementScreenState createState() =>
      _ProfileManagementScreenState();
}

class _ProfileManagementScreenState extends State<ProfileManagementScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenSetup(
      decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
      appBar: AppBar(
        title: Text('Profile Management'),
      ),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              VerticalSpace(20),
              SizedBox(
                width: 150,
                height: 150,
                child: Image.asset(
                  AssetImages.avatar,
                  fit: BoxFit.fitWidth,
                ),
              ),
              VerticalSpace(20),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: <Widget>[
                    CupertinoTextField(
                      placeholder: 'Username',
                    ),
                    VerticalSpace(20),
                    CupertinoTextField(
                      placeholder: 'Email',
                    ),
                    VerticalSpace(20),
                    CupertinoTextField(
                      placeholder: 'Phone Number',
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
